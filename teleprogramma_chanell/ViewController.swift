//
//  ViewController.swift
//  teleprogramma_chanell
//
//  Created by Виктория Хисма on 11.08.2022.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let cellID = "ChanelTableViewCell"
    var channel: [ChannelInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        let url = URL(string:host+url_channel+"city=1")!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let channels_data: JsonChannel = try! JSONDecoder().decode(JsonChannel.self, from: data)
            for channel in channels_data.data{
                self.channel.append(channel)
            }
//          обновление таблицы для отрисовки данных
            DispatchQueue.main.async {
            self.loadData();
            }
            
        
        }
        task.resume()
        
    }
    


}
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as! ChanelTableViewCell
        
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.orange.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 10
        cell.clipsToBounds = true
//      достаем картинку для отображения старт
        let imageUrl = URL(string:host+channel[indexPath.row].logo)!
        let imageData = try! Data(contentsOf:imageUrl)
//      достаем картинку для отображения стоп
//      отрисовка элементов старт
        let channel_element = channel[indexPath.row]
        cell.numLabel.text = "\(channel_element.lcn)"
        cell.ImageChanel.image = UIImage(data: imageData)
        cell.nameChanel.text = channel_element.name
//      отрисовка элементов стоп

        return cell
        
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection secrion:Int) -> Int{
        return self.channel.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "DataViewController")
            as? DataViewController{
            vc.channel_info = [channel[indexPath.row]]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func loadData() {
        // перезапуск табилцы
        tableView.reloadData()
    }
}

struct JsonChannel: Decodable {
    let status: Int
    let data:[ChannelInfo]
    let error: String?
}

struct ChannelInfo: Decodable {
    enum Category: String, Decodable {
        case swift, combine, debugging, xcode
    }

    let id: Int
    let comteza_id: Int
    let name: String
    let thematics_id: Int?
    let lcn: Int
    let logo: String
}

