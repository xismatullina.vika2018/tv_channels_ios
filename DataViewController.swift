//
//  DataViewController.swift
//  teleprogramma_chanell
//
//  Created by Виктория Хисма on 21.08.2022.
//

import UIKit
import Foundation


//enum ProfileViewModelItemType {
//   case nameAndPicture
//   case about
//   case email
//   case friend
//   case attribute
//}
//
//protocol ProfileViewModelItem {
//   var type: ProfileViewModelItemType { get }
//   var sectionTitle: String { get }
//   var rowCount: Int { get }
//   var isCollapsible: Bool { get }
//   var isCollapsed: Bool { get set }
//}
//
//extension ProfileViewModelItem {
//   var rowCount: Int {
//      return 1
//   }
//
//   var isCollapsible: Bool {
//      return true
//   }
//}


class DataViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var name_label: UILabel!
    
    
    var channel_info:[ChannelInfo] = []
    var program_info:[ProgramsInfo] = []
    let cellID = "ProgramTableViewCell"
    
    private func registerTableViewCells() {
        let textFieldCell = UINib(nibName: cellID,
                                  bundle: nil)
        self.tableView.register(textFieldCell,
                                forCellReuseIdentifier: cellID)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.registerTableViewCells()
        
        let channel_element = channel_info[0]
        let newDate = Int(Calendar.current.startOfDay(for: Date()).timeIntervalSince1970)
        
        let get_params = "city=1&"+"channel="+String(channel_element.id)+"&comteza_id="+String(channel_element.comteza_id)+"&data="+String(newDate)
        let url = URL(string:host+url_program+get_params)!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let program_data: Programs = try! JSONDecoder().decode(Programs.self, from: data)
            print(program_data.data)
            for program in program_data.data{
                self.program_info.append(program)
            }
            DispatchQueue.main.async {
            self.loadData();
            }
        }
        
//      обновление таблицы для отрисовки данных
        
        name_label.text=channel_element.name

        task.resume()

        
    }
    

    

}

extension DataViewController: UITableViewDelegate, UITableViewDataSource{
    
    func convertDate(dateValue: Int) -> String {
        let truncatedTime = Int(dateValue)
        let date = Date(timeIntervalSince1970: TimeInterval(truncatedTime))
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as! ProgramTableViewCell
        let program_element = program_info[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.layer.borderColor = UIColor.orange.cgColor
        cell.layer.borderWidth = 3
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
//        cell.numLabel.text = "test"
//        cell.nameLabel.text = "1"
        cell.accessoryType = .disclosureIndicator
        
      
        cell.numLabel.text = "\(self.convertDate(dateValue: program_element.start))"
        cell.nameLabel.text = String(program_element.name ?? "") + String(program_element.age_limit ?? "")
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection secrion:Int) -> Int{
        return program_info.count
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    func loadData() {
        // перезапуск табилцы
        tableView.reloadData()
    
    
}

struct Programs: Decodable {
    let status: Int
    let data:[ProgramsInfo]
    let error: String?
}

struct ProgramsInfo: Decodable {
    enum Category: String, Decodable {
        case swift, combine, debugging, xcode
    }

    let id: Int64 // id телепрограммы передаЧИ
    let age_limit: String? // возрастное ограничение
    let name: String? // название передачи
    let description: String? // описание
    let start: Int // начало времени передачи
    let stop: Int // конец времени передачи
}
}
