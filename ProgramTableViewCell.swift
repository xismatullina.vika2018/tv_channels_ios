//
//  ProgramTableViewCell.swift
//  teleprogramma_chanell
//
//  Created by Виктория Хисма on 12.09.2022.
//

import UIKit

class ProgramTableViewCell: UITableViewCell {
    
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
